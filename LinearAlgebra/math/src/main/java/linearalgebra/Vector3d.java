//sebastian fernandez martinez 2237597

package linearalgebra;

public class Vector3d {
    private double x;
    private double y;
    private double z;

    public Vector3d(double x, double y, double z){
        this.x = x;
        this.z = z;
        this.y = y;
    }

    public double getX(){
        return this.x;
    }
    public double getY(){
        return this.y;
    }
    public double getZ(){
        return this.z;
    }


    public double magnitude(){
        return Math.sqrt((this.x * this.x)+(this.y * this.y)+(this.z * this.z));
    }
    public double dotProduct(Vector3d vector){
        return ((this.x * vector.getX())+(this.y * vector.getY())+(this.z * vector.getZ()));
    }
    public Vector3d add(Vector3d vector){
        Vector3d newVector = new Vector3d(vector.getX()+this.x, vector.getY()+this.y, vector.getZ()+this.z);
        return newVector;
    }

    public String toString(){
        return this.x+", "+this.y+", "+this.z;
    }

} 
