// sebastian Fernnadez Martinez 2237597
package linearalgebra;

import static org.junit.Assert.*;

import org.junit.Test;

public class Vector3dTests {
    @Test
    public void testGetters() {
        Vector3d vector = new Vector3d(1, 1, 2);
        assertEquals(vector.getX(), 1,0);
        assertEquals(vector.getY(), 1,0);
        assertEquals(vector.getZ(), 2,0);
    }

    @Test
    public void testMagnitude(){
        Vector3d vector = new Vector3d(3, 4, 12);
        double magnitude = 13;
        assertEquals(vector.magnitude(), magnitude, 0);
    }
    @Test 
    public void testDotProduct(){
        Vector3d vector = new Vector3d(1, 1, 2);
        
        assertEquals(vector.dotProduct(new Vector3d(2,3,4)),13,0);
    }
    @Test
    public void testAdd(){
        Vector3d vector = new Vector3d(1, 1, 2);
        Vector3d vector2 = vector.add( new Vector3d(2,3,4));
        
        

        assertEquals(3, vector2.getX(),0);
        assertEquals(4, vector2.getY(),0);
        assertEquals(6, vector2.getZ(),0);
    }
}
